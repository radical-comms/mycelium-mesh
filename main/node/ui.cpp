/**
* @file ui.cpp
* @brief User interface functionality (console output, etc)
*/
using namespace std;

#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include <stdio.h>
#include "freertos/task.h"
#include <array>
#include <string.h>
#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <algorithm>
#include  <iomanip>
#include <bitset>

#include "../vars.h"
#include "crypto.h"
#include "../network/net-core.h"

extern struct packet p;

string uint_to_hex(uint8_t *input, uint8_t size, bool reverse = false){
  ostringstream convert;
  if (reverse == true){
    for (int i = size; i != 0; i--) {
      convert << std::setfill('0') << std::setw(2) << std::uppercase << std::hex << (int)input[i-1];
      if (i != 1){convert << ":";};
    }
  }else{
    for (int i = 0; i < size; i++) {
      convert << std::setfill('0') << std::setw(2) << std::uppercase << std::hex << (int)input[i];
      if (i != size-1){convert << ":";};
    }
  }
  string hexString = convert.str();
  //cout << convert.str() <<  << std::endl;
  return hexString;
}

void printKeys(){
  // Display the PUBLIC KEYS, only uncomment secret keys for troubleshooting.
  cout << "[printKeys]             | Signing Public Key: " << uint_to_hex(myKeys.pub_sign, 32) << endl;
  cout << "[printKeys]             | Key-Exchange Public Key: " << uint_to_hex(myKeys.pub_kx, 32) << endl;
  cout << "[printKeys]             | Pubkeys Fingerprint: " << uint_to_hex(myIdentity.fingerprint, 16) << endl;
  cout << "[printKeys]             | Persistant Packet Counter: " << myCounter.u32 << endl;
  //cout << "sign Secret Key: " << uint_to_hex(myKeys.priv_sign, 64) << endl;
  //cout << "box Secret Key: " << uint_to_hex(myKeys.priv_box, 32) << endl;
  //cout << "kx Secret Key: " << uint_to_hex(myKeys.priv_kx, 32) << endl;
}
