/**
* @file pm.cpp
* @brief Power Management & Power Saving functionality
*/



#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_pm.h"
#include "esp_clk.h"


esp_pm_lock_handle_t pm_lock;

void autosleep_setup(){
  esp_pm_config_esp32_t pm_config = {
      .max_freq_mhz = 240,
      .min_freq_mhz = 40,
      .light_sleep_enable = true
   };

  esp_pm_configure(&pm_config);
  esp_pm_lock_create(ESP_PM_NO_LIGHT_SLEEP, 1, NULL, &pm_lock);
};


void autosleep_hold(){
esp_pm_lock_acquire(pm_lock);
}

void autosleep_release(){
  esp_pm_lock_release(pm_lock);
}
