/**
* @file crypto.h
* @brief Probably going to merge this with boot.(h|cpp) in the future.
*/

#pragma once
using namespace std;
extern "C" {
	#include "sodium.h"
}


extern size_t size16_t;
extern size_t size32_t;
extern size_t size64_t;
extern size_t size128_t;
extern size_t size160_t;
extern size_t size224_t;

union asym_keypairs{
	struct{
		uint8_t pub_sign[crypto_sign_PUBLICKEYBYTES];	//32B
		uint8_t pub_kx[crypto_kx_PUBLICKEYBYTES];			//32B
		uint8_t priv_sign[crypto_sign_SECRETKEYBYTES];//64B
		uint8_t priv_kx[crypto_kx_SECRETKEYBYTES];		//32B

		};
	uint8_t pub_blob[64];
	uint8_t all_blob[160];
};

union identity{
	struct{
		// Probably implement an optional "Name" field at some point
		uint8_t pub_sign[crypto_sign_PUBLICKEYBYTES];	//32B
		uint8_t pub_kx[crypto_kx_PUBLICKEYBYTES];		//32B
		uint8_t signed_fingerprint[64];					//64B
		union{
			uint8_t fingerprint[16];						//16B
			uint64_t fingerprint64;
		};
	};
	uint8_t tx_blob[128];
};

union packet_counter{
	uint32_t u32;
	uint8_t u8[4];
};

extern asym_keypairs myKeys;
extern identity myIdentity;
extern packet_counter myCounter;
extern packet_counter mySavedCounter;


void verify_fingerprint(identity ident);
void save_myCounter();
void generate_identity();
