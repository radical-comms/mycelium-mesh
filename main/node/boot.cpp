/**
* @file boot.cpp
* @brief Functions Initialization of Non-Volitile storage, and
*  for generating cryptographic keys and initial counter if this
*  is the first boot (determined by the lack of those values within
*  NVS storage.)
*/

using namespace std;
#include <cstdio>
#include <iostream>
#include <stdio.h>
#include <cstdint>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

#include "ui.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "crypto.h"

extern "C" {
	#include "sodium.h"
}

/**
* @brief Initialize the NVS Flash.
* Initializes the NVS Flash:
* - Checks to see if nvs_flash_init(); returns ESP_OK.
* - If it does not, finds the nvs partition and "formats" it.
* - Finally, checks that nvs handle can be opened.
*/

nvs_handle nvs_init(){
  esp_err_t err = nvs_flash_init();
  if(err != ESP_OK){
    //If we don't get ESP_OK returned, find and format the NVS partition.
    const esp_partition_t* nvs_partition =
    esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, NULL);
    if(!nvs_partition) printf("[nvs_init]              | FATAL ERROR: No NVS partition found\n");
    err = (esp_partition_erase_range(nvs_partition, 0, nvs_partition->size));
    if(err != ESP_OK) printf("[nvs_init]               | FATAL ERROR: Unable to erase the partition\n");
  }
  //Now we try to open NVS storage
  nvs_handle my_handle;
  err = nvs_open("storage", NVS_READWRITE, &my_handle);
  if (err != ESP_OK) printf("[nvs_init]              | FATAL ERROR: Unable to open NVS\n");
  printf("[nvs_init]              | NVS Initialized!\n");
	return my_handle;
}

/**
* @brief Generates keys on first-boot, and saves them to nvs flash.
* kx_keypair is for diffe-hellman key Exchange.
* sign_keypair is for signing things.
* No async encryption keypair is needed, since kx_keypair can make us
* a symmetric key for any node we want to talk to encryptedly.
*/

void firstboot_keys(){
	nvs_handle my_handle = nvs_init();
	crypto_kx_keypair(myKeys.pub_kx, myKeys.priv_kx);
	crypto_sign_keypair(myKeys.pub_sign, myKeys.priv_sign);
	printf("[firstboot_keys]        | Node keypairs generated for the first time!\n");
	nvs_set_blob(my_handle, "myKeys", myKeys.all_blob, size160_t);
	nvs_flash_deinit();
}

/**
* @brief Creates our Packet Counter on first-boot only.
* myCounter always starts at 128 on firstboot to ensure validity.
* If myCounter is less than 128, something isn't working as it should.
*/

void firstboot_counter(){
	nvs_handle my_handle = nvs_init();
	myCounter.u32 = {128};
	nvs_set_blob(my_handle, "myCounter", myCounter.u8, size32_t);
	nvs_get_blob(my_handle, "myCounter", myCounter.u8, &size32_t);
	if (myCounter.u32 <= 127){printf("Counter invalid, time to die."); vTaskDelay(50); abort();}
	nvs_flash_deinit();
}

/**
* @brief Initializes some of the node's important variables, on every boot.
* - Loads keypairs and counter from nvs flash.
* - If loading fails with "ESP_ERR_NVS_NOT_FOUND", assumes this is the first boot
*		and runs firstboot functions.
* - If loading fails with some other error message, someting is wrong, and abort.
* - Finally, run generate_identity();.
*/

void node_init(){
	  nvs_handle my_handle = nvs_init();
		//We will get our Keys from the NVS, or generate them, or fail miserably!
		esp_err_t err = nvs_get_blob(my_handle, "myKeys", myKeys.all_blob, &size160_t);
  	esp_err_t err2 = nvs_get_blob(my_handle, "myCounter", myCounter.u8, &size32_t);
		if (err == ESP_OK && err2 == ESP_OK){
			printf("[node_init]             | Keys loaded from NVS flash.\n");
		}else if (err == ESP_ERR_NVS_NOT_FOUND && err2 == ESP_ERR_NVS_NOT_FOUND){
			  firstboot_keys();
				firstboot_counter();
		} else {printf("[node_init]             | NVS-related error occured!\n[node_init]        |  This is bad! Please try re-flashing device.");
		abort();}
		nvs_flash_deinit();
		generate_identity();
}
