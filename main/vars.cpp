/**
* @file vars.cpp
* @brief Set constants and global structs
*/


using namespace std;
#include <stdint.h>
#include <inttypes.h>
#include <queue>
#include <vector>
#include <array>


/**
* @brief Queue of non-reusable random values
* This global queue can be consumed by anything requiring strong randomness.
* Each value should be removed upon use. entropyTask will refill it
* automatically.
*/
queue<uint8_t> entropyQueue;

/**
* @brief Data representing a neighboring node
* Contains the address of the node, and the last time a hearbeat
* was received from it.
*/
struct neighbor{
	uint32_t address;
	int64_t lastTime;
	bool operator==(const neighbor& n) const { return address == n.address; }
	bool operator!=(const neighbor& n) const { return address != n.address; }
	bool operator<=(const neighbor& n) const { return lastTime <= n.lastTime; }
	bool operator>=(const neighbor& n) const { return lastTime >= n.lastTime; }
};

/**
* @brief Address of a LoRa node
*
* The address can be treated as a single 32bit number or an array of 4 octets
*/
union loraAddress{
	uint32_t ui32;
	uint8_t ui8[4];
};

/**
* @brief Global flag indicating whether device is currently transmitting
*
* Before transmitting, check this value to prevent a collision.
* Immediately before transmitting, set this value to true.
* Immediately after transmitting, set to false.
* While listening, check this value to avoid recieving your own transmissions.
*/
