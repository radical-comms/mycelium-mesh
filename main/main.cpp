/**
* @file main.cpp
* @brief Init code and main tasks
*/
using namespace std;
#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include <stdio.h>
#include "freertos/task.h"
#include <array>
#include <string.h>
#include <iostream>
#include <cstring>
#include <sstream>
#include <cassert>


extern "C" {
	#include "network/lora.h"
	#include "sodium.h"
	#include "ssd1306.h"
	#include "ssd1306_draw.h"
	#include "ssd1306_font.h"
	#include "oled/ssd1306_default_if.h"
   	void app_main(void);
}
#include "vars.h"
#include "network/config.h"
#include "network/net-core.h"
#include "network/net-relay.h"
#include "node/boot.h"
#include "node/ui.h"
#include "node/oled.h"
#include "node/pm.h"
#include "esp32/clk.h"
#include "endian.h"
extern struct SSD1306_Device I2CDisplay;

/**
* @brief Defines the mem address for ESP32's Random Number Generator.
* Used for entropyTask().
*/

#define DR_REG_RNG_BASE                     0x3ff75144

/**
* @brief Defines the register location on lora module for last payload length.
* Used in loraListener().
*/

#define REG_PAYLOAD_LENGTH        0x22

/**
* @brief Task to listen for and process incoming LoRa transmissions
*
* Sets LoRa radio to receive mode. When a packet is received:
* - Determine the size and write the packet to memory
* - Extract the nonce
* - Validate the network encryption
* - Send the decrypted message to be processed by packetHandler
*/
void loraListener(void *p){
   for(;;) {
      if(txLock==false){
    	  lora_receive();    // put into receive mode
    	  while(lora_received()) {
					packet myPacket;
					myPacket.receive();
					lora_receive();
					//autosleep_hold();
					//vTaskDelay(20);
					//autosleep_release();
    	  };
				vTaskDelay(100);
      };
   };
};


/**
* @brief Task for populating the entropy queue with values
*
* Uses RF noise to populate entropyQueue with random values
* for use in cryptographic functions. As values are removed
* from entropyQueue, this task adds fresh ones.
*
* TODO: Use WiFi Module to produce entropy, presently it is
* software-generated.
*/
void entropyTask(void *p){
	 //UBaseType_t uxHighWaterMark;
	for(;;) {
		//uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		if((entropyQueue.size() < 128)){
			cout << "[entropyTask]           | Random number queue low, refilling it." << endl;
			while (true) {
				uint32_t randomNumber = READ_PERI_REG(DR_REG_RNG_BASE);
				entropyQueue.push((uint8_t)(randomNumber>>24));
				entropyQueue.push((uint8_t)(randomNumber>>16));
				entropyQueue.push((uint8_t)(randomNumber>>8));
				entropyQueue.push((uint8_t)(randomNumber>>0));
				if (entropyQueue.size() > 1024){
					//uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
					//cout << "Stack Size Free:" << uxHighWaterMark << endl;
					break;
				}
				vTaskDelay(5);
			}

		}
		vTaskDelay(10);
	}
}

/**
* @brief Task for sending a heartbeat to neighbor nodes
*
* Sends heartbeat transmissions to notify neighboring nodes that
* it's in range. Also prunes old entries from the Neighbor Table.
*/
void neighborHello (void *p){
	for(;;) {
		vTaskDelay(pdMS_TO_TICKS(10000));
		netMGMT_txHello();
		//prune_neighborTable();
	}
}

void neighborMgmt (void *p){
	for(;;) {
		vTaskDelay(pdMS_TO_TICKS(1000));
		//prune_neighborTable();
	}
}

void loraSender (void *p){
	for(;;) {
		while (tx_buffer.empty() == false){
			tx_transmit();
		}
		vTaskDelay(100);
	}
}

void oledTask (void *p){
	if ( DefaultBusInit( ) == true ) {
		 printf( "BUS Init lookin good...\n" );
		 printf( "Drawing.\n" );
		 oledSetup( &I2CDisplay, &Font_droid_sans_fallback_15x17);
		 oledPrint( &I2CDisplay, "Hello i2c!" );
	for(;;) {
		const char * c = uint_to_hex(myCounter.u8, 4, true).c_str();
		oledPrint( &I2CDisplay, c);
		}
		vTaskDelay(1000);
	}
}

/**
* @brief Main function executed when the hardware is powered on
*
* Sets initial configurations for how the radio will operate,
* and starts tasks which run continuously. Each task runs a
* corresponding function.
*/
void app_main(void)
{
		 node_init();
		 printKeys();
	   cout << "[app_main]              | Loading Lora " << lora_init() << endl;
		 lora_set_bandwidth(125E3);
	   lora_set_frequency(417e6);
		 lora_set_spreading_factor(8);
		 lora_set_tx_power(17);
	   lora_set_coding_rate(5);
	   lora_enable_crc();
		 autosleep_setup();
	   cout << "[app_main]              | Loading Sodium " << sodium_init() << endl;
	   xTaskCreate(&loraListener, "loraListener", 4096, NULL, 5, NULL);
	   xTaskCreate(&entropyTask, "entropyTask", 2048, NULL, 5, NULL);
	   xTaskCreate(&neighborHello, "neighborHello", 4096, NULL, 5, NULL);
		 xTaskCreate(&neighborMgmt, "neighborMgmt", 4096, NULL, 5, NULL);
		 xTaskCreate(&loraSender, "loraSender", 4096, NULL, 5, NULL);
		 xTaskCreate(&oledTask, "oledTask", 2048, NULL, 5, NULL);
}
