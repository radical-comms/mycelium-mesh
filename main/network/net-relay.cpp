/**
* @file network-relay.cpp
* @brief Functions for managing relay nodes,
*  neighbor tables, message storage, etc.
*/
using namespace std;
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <limits.h>
#include <iostream>
#include <cstring>
#include <sstream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <string>
#include <map>

extern "C" {
}
#include "config.h"
#include "../vars.h"
#include "net-core.h"
#include "esp_event.h"
#include "net-relay.h"





void netMGMT_addNeighbor(uint64_t nid){
//	if(NeighborTable.count(nid)>0){
//			cout << "neighbor timer updated in neighborTable";
//		NeighborTable.at(nid) = esp_timer_get_time();
//	}else{
//		  cout << "neighbor added to neighborTable";
//	};
};

void netMGMT_pruneNT(){

};


/**
* @brief Prune dead neighbors from the neighbor table
*
* Iterate through each neighbor in the neighbor table and delete it if it's been
* too long since a heartbeat has been seen
*/


/**
* @brief Send a heartbeat to the broadcast address
*
* Sends a message of type Network Management Hello to the broadcast address
* All nodes which receive this message should update their neighbor tables
*/

void netMGMT_txHello(){
	cout << "                         ☎  Sending NetMGMT Hello ☎ " << endl;
	vector<uint8_t> myPayload;
	myPayload.push_back(12);
	myPayload.push_back(12);
	myPayload.push_back(12);
	myPayload.push_back(12);
	myPayload.push_back(12);
	myPayload.push_back(12);
	packet myPacket;
	myPacket.send(myPayload, 6);
	tx_buffer.push(myPacket);
};


void netMGMT_rxHello(uint8_t *srcAddress){
/*	loraAddress addr;
	addr.ui8[0] = srcAddress[0];
	addr.ui8[1] = srcAddress[1];
	addr.ui8[2] = srcAddress[2];
	addr.ui8[3] = srcAddress[3];
	uint32_t addr32 = addr.ui32;

	loraAddress f = find(neighborTable.begin(),neighborTable.end(),addr32);
	if (f != neighborTable.end()){
		neighborTable.erase(addr32);
		neighbor ourNeighbor = {addr.ui32, esp_timer_get_time() + deadTimer };
		neighborTable.push_back (ourNeighbor);
	}else{
		neighbor ourNeighbor = {addr.ui32, esp_timer_get_time() + deadTimer };
		neighborTable.push_back (ourNeighbor);
	} */
}
