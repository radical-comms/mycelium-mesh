## Please note, this software is currently Pre-Alpha and cannot be used to send messages yet.

# Getting Started

## Building

(These instructions currently broken since migration from gitlab.com, use regular esp-idf compilation for now)

Code is automatically built by Gitlab upon commit.  After pushing your commit, check here to see if the build passed: https://gitlab.com/RadDevs/myceliumesp-cpp/pipelines

## Flashing 
(These instructions currently broken since migration from gitlab.com, use regular esp-idf compilation for now)

To flash your code to a local ESP device, you will need to run a local build environment.
Follow these steps:
- Install docker
- Plug in your ESP device via USB and turn it on
- Run `./docker.sh` to build, flash and monitor the output of your code
  - If it fails, you may need to add these options:
    - Set your device's [USB port](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html#step-6-connect-your-device): `./docker.sh -p [path to USB port]`
    - Set the path to your project code: `./docker.sh -d [path to project code]`

# Project Overview

mycelium
Off-Grid Text Messaging for Autonomous Communities

Problem

Corporate internet and cellular networks are high performance and have widespread access, but they cannot be relied on. Internet and phone access can easily become unavailable when we need it most, due to natural disasters or interference from authorities.  The danger of being cut off from basic communications poses a threat to the safety and effectiveness of autonomous communities.



Concept

mycelium: An off-grid radio network for digital messaging in a region or metro area using LoRa transceivers and ESP32 microcontrollers. The network consists of solar powered relay nodes which are placed in public spaces and high-up locations. And peer nodes which people carry with them, or keep in a building, house, or vehicle, through which they can send and receive messages using their phone, using any peer node. mycelium operates completely independently from the grid, staying functional when cellular phone, text, and internet, or even electricity are unavailable.



Design Priorities

Asynchronous: Messages should be cached in the network for users, in case they are offline or out of coverage area.

Attack-resistant: The functionality of the network should be resistant to attempts to disrupt functionality.

Low Power: Nodes cannot depend on the power grid, they must be able to run on modest solar, wind or battery power.

Range over speed: While WiFi mesh networks prioritize high-speed access in dense local environments, mycelium is designed to cover much longer distances at lower speeds.

Privacy: The network should be encrypted and communications should be authenticated.

Sustainability: Upkeep for the network should be low-effort, and it should be maintainable by non-experts.

Cheap: Peer nodes should be cheap enough to widely distribute to entire communities, and relay nodes should be cheap enough to build a robustly interconnected network.

Open: Folks should be free to use the code and hardware designs as they will.

Multilingual: We should ensure that multilingual character sets are used every step of the way, furthermore... any text-compression used should be useful in multiple languages.

Content-Based Network: At least for now, end-to-end addressing doesn't really matter ¯\_(ツ)_/¯



How it works

Your phone connects to a small mycelium Peer Node, either through WiFi, Bluetooth or a wire.

You use your phone to compose and send an encrypted message to either specific people or a groups.

The message is relayed from your Peer Node to the nearest Relay Node.

The Relay Node(s) are located preferrably in high up locations, for example on top of a mountain or skyscraper. They are configured and deployed by local volunteers.

The message is broadcast intelligently throughout the network until all Relay Nodes have a copy of the message.

The encrypted message is stored in a FIFO buffer on all of the Peer Nodes.

Peer Nodes periodically ask their local Relay Nodes if they have any new messages, they will then receive the message from the Peer Node and decrypt it.



Status

The mycelium Project is still in it's early stages of development and welcomes code contributions, help on the documentation wiki, testing, etc...

